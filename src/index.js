import React from 'react';
import ReactDOM from 'react-dom';

/* import method 01 */
import { getUser, getUsers, createUser, deleteUser, updateUser } from './services.js';

/* import method 02
import crudService from './services.js'; */

class Application extends React.Component {
    componentDidMount() {
        getUsers()
        .then((users) => {
            console.log('got all users');
        })
        .catch((e) => {
            console.error('oops', e);
        });
    }
    
    render() {
        return (
            <div>Application loaded...</div>
        );
    }
}

const el = document.getElementById('app');
ReactDOM.render(<Application />, el);