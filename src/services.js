/** Create (Crud) */
export const createUser = (data) => new Promise((resolve, reject) => {
    const url = `${window.location.href}/api/users`;
    const req = new XMLHttpRequest();
    req.responseType = 'json';
    req.open('POST', url, true);
    req.onload = (ev) => {
        resolve(ev.response);
    };
    req.send(JSON.stringify(data));
});

/** Read (cRud) */
export const getUsers = () => new Promise((resolve, reject) => {
    const url = `${window.location.href}/api/users`;
    const req = new XMLHttpRequest();
    req.responseType = 'json';
    req.open('GET', url, true);
    req.onload = (ev) => {
        resolve(ev.response);
    };
    req.send();
});

/** Read (cRud) */
export const getUser = (id) => new Promise((resolve, reject) => {
    const url = `${window.location.href}/api/users/${id}`;
    const req = new XMLHttpRequest();
    req.responseType = 'json';
    req.open('GET', url, true);
    req.onload = (ev) => {
        resolve(ev.response);
    };
    req.send();
});

/** Update (crUd) */
export const updateUser = (id, data) => new Promise((resolve, reject) => {
    const url = `${window.location.href}/api/users/${id}`;
    const req = new XMLHttpRequest();
    req.responseType = 'json';
    req.open('PUT', url, true);
    req.onload = (ev) => {
        resolve(ev.response);
    };
    req.send(JSON.stringify(data));
});

/** Delete (cruD) */
export const deleteUser = (id) => new Promise((resolve, reject) => {
    const url = `${window.location.href}/api/users/${id}`;
    const req = new XMLHttpRequest();
    req.responseType = 'json';
    req.open('DELETE', url, true);
    req.onload = () => {
        resolve();
    };
    req.send();
});

export default {
    create: createUser,
    read: getUser,
    readAll: getUsers,
    update: updateUser,
    delete: deleteUser,
};
